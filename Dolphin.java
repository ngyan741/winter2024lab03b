public class Dolphin{
	
	public String species;
	public String color;
	public double jump_height;
	public boolean isEndangered; 
	
	public void introduceDolphin(){
		System.out.println ("This is a " + species + " dolphin, and it is " + color + ".");
	}
	
	public void advocateProtection(){
		if(this.isEndangered){
			System.out.println("The " + species + " dolphin is endangered, please help protect it! Humans contribute to this by overfishing, as well as water pollution and driving them out of their habitat.");
		}
		else{
			System.out.println("Thankfully, the " + species + " dolphin is not endangered, but we should still advocate to stop harming dolphins and destroying their natural habitats.");
		}
	}
	
	public void leap(){
		System.out.println("The dolphin leaped " + jump_height + " feet into the air!");
	}
	
}