import java.util.Scanner;
public class VirtualPetApp{
	
	public static void main(String args[]){
	
		Scanner reader = new Scanner(System.in);
		
		Dolphin[] pod = new Dolphin[1];
		for(int i=0; i<pod.length; i++){
			pod[i] = new Dolphin();
			System.out.println("Enter dolphin species:");
			pod[i].species = reader.nextLine();
			System.out.println("Enter the jump height(in feet):");
			pod[i].jump_height = Double.parseDouble(reader.nextLine());
			System.out.println("Enter dolphin color:");
			pod[i].color = reader.nextLine();
			System.out.println("True or False, Enter whether the dolphin is endangered or not:");
			pod[i].isEndangered = Boolean.parseBoolean(reader.nextLine());
		}
		
		System.out.println("Species: " + pod[pod.length-1].species);
		System.out.println("Jump Height: " + pod[pod.length-1].jump_height + " ft");
		System.out.println("Color: " + pod[pod.length-1].color );
		System.out.println("Endangered: " + pod[pod.length-1].isEndangered + "\n");
		
		pod[pod.length-1].leap();
		pod[pod.length-1].introduceDolphin();
		pod[pod.length-1].advocateProtection();
		
	}
}